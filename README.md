##Video Transcoding Webservice##

Purely educational project for online video files transcoding and storage leveraging AWS and ElasticTranscoder in particular.
Has a very rudimentary API sufficient, however, for a simple video blogging website.
The front end is developed by the other team within the Multimedia Course @ CSUN. 